<?php

namespace AppBlog\AdminBundle\Controller;

use AppBlog\AdminBundle\Entity\Post;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Smfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\BooleanType;

/**
 * Post controller.
 *
 */

 class PostController extends Controller
 {
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('AppBlogAdminBundle:Post')->findAll();
        $author = 'Bijay shreshta';
        
        return $this->render('@AppBlogAdmin/post/index.html.twig', array(
            'posts' => $posts,
            'author' => $author
        ));
    }


    public function saveAction(){
        $em = $this->getDoctrine()->getManager();

        $post= new Post();
        $post->setTitle("My second blog")
             ->setDescription("I am currently living here")
             ->setCreatedAt(new \Datetime())
             ->setStatus(true);
            

       //$em->persist($post);
        //$em->flush();
        
        return new Response('Datas are saved');

            }
 }