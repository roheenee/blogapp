<?php


namespace AppBlog\AdminBundle\Controller;

use AppBlog\AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Email;


class RegistrationController extends Controller
{
    public function registerAction(Request $request,UserPasswordEncoderInterface $passwordEncoder )
    {
        // 1) build the form
        $user = new User();
        $form = $this->createFormBuilder($user)
            ->add('username', TextType::class, array('attr' =>
                array('class' => 'form-control')))
            ->add('email', EmailType::class, array('attr' =>
                array('class' => 'form-control')))
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
            ])

            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('@AppBlogAdmin/registration/register.html.twig', array(

            'form' => $form->createView(),
        ));
        }




}