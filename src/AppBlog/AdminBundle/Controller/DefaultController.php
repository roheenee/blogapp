<?php

namespace AppBlog\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppBlogAdminBundle:Default:index.html.twig');
    }
}
